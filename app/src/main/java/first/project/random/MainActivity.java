package first.project.random;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    EditText editText2, editText3;
    Button randomButton;
    TextView textView2;

    Random r;
    int min, max, output;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        r = new Random();

        editText2 = (EditText) findViewById(R.id.editText2);
        editText3 = (EditText) findViewById(R.id.editText3);
        randomButton = (Button) findViewById(R.id.randomButton);
        textView2 = (TextView) findViewById(R.id.textView2);

        randomButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tempMin, tempMax;
                tempMin = editText2.getText().toString();
                tempMax = editText3.getText().toString();
                if (!tempMin.equals("") && !tempMax.equals("")) {
                    min = Integer.parseInt(editText2.getText().toString());
                    max = Integer.parseInt(editText3.getText().toString());
                    if (max > min) {
                        output = r.nextInt((max - min) + 1) + min;
                        textView2.setText("" + output);
                    }
                }
            }
        });

    }
}
